/*
 * Copyright © 2015-2017 Roger Leigh <rleigh@codelibre.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 */

#pragma once

#include <codelibre/multiarray/space.h>

namespace codelibre
{
  namespace multiarray
  {

    /**
     * Create a space without any storage order or subrange components.
     *
     * This helper function takes the specified space, and creates a
     * new space with two modifications.  Firstly, the extents of each
     * dimension are made size of the subrange of the source space, or
     * are left the extent size, depending upon @c squash_extents.
     * The subrange is removed in both cases.  Secondly, the storage
     * order is stripped out, making it the same as the logical order
     * if specified with @c strip_storage..
     *
     * This transformation is useful in order to conveniently
     * determine the valid coordinates for use with a space using
     * either subranges or different storage orders: simply call the
     * Space::coord method for each valid index in the range [0,
     * Space::num_elements()).  This doesn't work when subranges or
     * different storage orders are in use because the valid indices
     * may fall outside this range.
     *
     * @param space the source space.
     * @param squash_extents @c true to make the extent size the size
     * of the subrange, or @c true to leave the same as the extent
     * size.
     * @param strip_storage @c true to strip the storage order or @c
     * false to retain it.
     * @returns the modified space.
     */
    template<class Space>
    Space
    make_logical_space(const Space& space,
                       bool         squash_extents,
                       bool         strip_storage)
    {
      typename Space::logical_order logical_extents;
      for (const auto& i : space.get_logical_order())
        {
          if (squash_extents)
            logical_extents.push_back(dimension(i.end - i.begin));
          else
            logical_extents.push_back(dimension(i.extent));
        }
      if (strip_storage)
        return Space(logical_extents);
      else
        return Space(logical_extents, space.storage_order());
    }

  }
}

/*
 * Local Variables:
 * mode:C++
 * End:
 */
