/*
 * Copyright © 2015-2017 Roger Leigh <rleigh@codelibre.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 */

#pragma once

#include <array>
#include <cassert>
#include <map>
#include <set>

#include <codelibre/multiarray/storage.h>

namespace codelibre
{
  namespace multiarray {

    template<typename T, int Size>
    using array_coord = std::array<T, Size>;

    template<class charT, class StreamTraits, typename T, int Size>
    inline std::basic_ostream<charT, StreamTraits> &
    operator<<(std::basic_ostream<charT, StreamTraits> &os,
               const array_coord<T, Size> &coord) {
      os << '(';
      for (auto i = coord.cbegin();
           i != coord.cend();
           ++i) {
        os << *i;
        if (i + 1 < coord.cend())
          os << ',';
      }
      os << ')';
      return os;
    }

    /**
     * Fixed-rank coordinate.
     *
     * @tparam T the integer type representing the position in a single dimension.
     * @tparam Rank the number of dimensions.
     */
    template<typename T, int Rank>
    using array_coord = std::array<T, Rank>;

    /**
     * Variable-rank coordinate.
     *
     * @tparam T the integer type representing the position in a single dimension.
     */
    template<typename T>
    class vector_coord : public std::vector<T> {
      using std::vector<T>::vector;
    };

    template<class charT, class StreamTraits, typename T>
    inline std::basic_ostream<charT, StreamTraits> &
    operator<<(std::basic_ostream<charT, StreamTraits> &os,
               const vector_coord<T> &coord) {
      os << '(';
      for (auto i = coord.cbegin();
           i != coord.cend();
           ++i) {
        os << *i;
        if (i + 1 < coord.cend())
          os << ',';
      }
      os << ')';
      return os;
    }

      /**
       * Traits for a fixed-rank array or span.
       *
       * @tparam Rank the number of dimensions.
       */
    template<int Rank>
    struct array_traits
    {
      /// List type for static lists.
      template<typename T>
      using list_type = std::array<T, Rank>;

      /// Coordinate type for static lists.
      template<typename T>
      using coord_type = array_coord<T, Rank>;

      template<typename T>
      static auto
      create_array(typename list_type<T>::size_type rank) -> list_type<T>
      {
        assert(Rank == rank);

        return list_type<T>{};
      }
    };

      /**
       * Traits for a variable-rank array or span.
       */
    struct vector_traits
    {
      /// List type for dynamic lists.
      template<typename T>
      using list_type = std::vector<T>;

      /// Coordinate type for dynamic lists.
      template<typename T>
      using coord_type = vector_coord<T>;

      template<typename T>
      static auto
      create_array(typename list_type<T>::size_type size) -> list_type<T>
      {
        return list_type<T>{size};
      }
    };

    /**
     * Collection of dimensions.
     *
     * This class implements the calculations needed to convert
     * between a logical linear array index and logical coordinates in
     * each dimension, and between logical coordinates and linear
     * storage array index.
     *
     * The concept implemented is that an abstrace space is set of
     * dimensions in a defined logical order.  This logical order is
     * the order in which the end user or programmer will index
     * dimensions, for example the coordinates to refer to a specific
     * location in (x,y,z) might be (0,4,2).  The logical order
     * however, need not be the same as the storage order (the
     * physical layout in memory).  The storage order may be specified
     * independently, and allows for the logical dimensions to be
     * stored in an arbitrary order, each of which may progress in a
     * forward or reverse direction.
     *
     * For the logical order and storage order, the stride for each
     * dimension and base index are computed, to allow for fast
     * translation between linear logical and storage indices and
     * logical coordinates as an intermediate representation between
     * the two.
     *
     * This class does not implement any storage; it is purely for
     * implementing the calculations needed by a multi-dimensional
     * array class or for any other calulation involving multiple
     * dimensions.
     *
     * @see Boost.MultiArray for a compile-time templated alternative
     * which will likely have higher performance, with the caveat that
     * it requires the number of dimensions to be fixed at compile
     * time.  This class trades off performance for run-time
     * flexibility.
     */
    template<class ArrayTraits, class DimensionType = dimension>
    class basic_space
    {
    public:
      /// Dimension type.
      using dimension_type = DimensionType;
      /// Traits.
      using traits_type = ArrayTraits;
      /// Size type for dimension extents.
      using size_type = typename dimension_type::size_type;
      /// Index type for dimension indices.
      using index_type = typename dimension_type::index_type;
      /// Signed difference type for difference between dimension indices.
      using difference_type = typename dimension_type::difference_type;
      /// List type for dimension-sized lists.
      template<typename T>
      using list_type = typename ArrayTraits::template list_type<T>;
      /// List type for dimension-sized coordinates..
      template<typename T>
      using generic_coord_type = typename ArrayTraits::template coord_type<T>;

      /// Index type for coordinate within a dimension.
      using coord_type = generic_coord_type<index_type>;
      /// Signed difference type for difference between coordinates.
      using coord_difference_type = generic_coord_type<difference_type>;

      using detail_type = storage_detail<typename dimension_type::traits_type>;

      using indexed_storage_type = indexed_storage<typename dimension_type::traits_type>;
      using indexed_subrange_type = indexed_subrange<typename dimension_type::traits_type>;

      /// Dimensions in logical order.
      using logical_order = list_type<dimension>;
      /// Dimensions in storage order (by index).
      using indexed_storage_order = list_type<indexed_storage_type>;
      /// Storage order details.
      using detail_list = list_type<detail_type>;
      /// Dimension extents subrange (by index).
      using indexed_subrange_list = list_type<indexed_subrange_type>;

    private:
      /// Dimensions in logical order (user-addressable order).
      logical_order _logical_order;
      /// Dimensions in indexed storage order (describing physical memory layout).
      indexed_storage_order _storage_order;
      /// Dimension details in logical order (describing physical strides and offsets).
      detail_list _detail;
      /// Base offset.
      index_type _base;

    public:
      /**
       * Construct with default storage order.
       *
       * The logical dimension order and storage order may not be
       * modified after construction.  The storage order (physical
       * memory layout) defaults to the logical dimension order, with
       * each dimension having ascending progression.
       *
       * @param dimensions the dimensions contained in this space, in
       * logical order.
       */
      explicit
      basic_space(const logical_order& dimensions):
        basic_space(dimensions, default_storage_order(dimensions))
      {
      }

      /**
       * Construct with indexed storage order.
       *
       * The logical dimension order and storage order may not be
       * modified after construction.  The storage order is specified
       * in terms of the index each dimension in the @c dimensions
       * parameter.
       *
       * @param dimensions the dimensions contained in this space, in
       * logical order.
       * @param order the storage order (by dimension index in @c
       * dimensions).  Dimension indices must not be repeated.
       * @throws std::logic_error if the unique constraint is
-      * violated.
       */
      basic_space(const logical_order&         dimensions,
                  const indexed_storage_order& order):
        _logical_order(dimensions),
        _storage_order(storage_order(dimensions, order)),
        _detail(ArrayTraits::template create_array<detail_type>(dimensions.size())),
        _base(0U)
      {
        compute_storage_strides();
        compute_storage_offset();
      }

    private:
      /**
       * Validate logical dimension order.
       *
       * @param dimensions the dimensions contained in this space, in
       * logical order.
       * @throws std::logic_error if invalid.
       */
      static void
      validate(const logical_order& dimensions)
      {
      }

        /**
         * Validate indexed storage order.
         *
         * Check for an invalid number of dimensions, invalid and duplicate dimension indices.
         *
         * @param order the storage order (by dimension index in @c
         * dimensions).  Dimension indices must not be repeated.
         * @throws std::logic_error if invalid.
         */
        static void
        validate(const logical_order&         dimensions,
                 const indexed_storage_order& order)
        {
          if (order.size() != dimensions.size())
          {
            boost::format fmt("Storage order dimension count %1% for order does not match space dimension count %2%");
            fmt % order.size() % dimensions.size();
            throw std::logic_error(fmt.str());
          }

          // Check for duplicates using set of indices.
          std::set<index_type> used;
          for (index_type i = 0; i < order.size(); ++i)
          {
            const auto& o(order.at(i));
            // Check dimension is valid.
            if (o.index >= dimensions.size())
            {
              boost::format fmt("Storage order dimension %1% index %2% out of valid range %3%–%4%");
              fmt % i % o.index % 0 % (dimensions.size() -1);
              throw std::logic_error(fmt.str());
            }
            // Check for duplicate dimensions.
            if (used.find(o.index) != used.end())
            {
              boost::format fmt("Dimension %1% used multiple times");
              fmt % o.index;
              throw std::logic_error(fmt.str());
            }
            used.insert(o.index);
          }
        }

      /**
       * Compute default storage order.
       *
       * By default, the storage order is the same as the logical order.
       */
      static auto
      default_storage_order(const logical_order& dimensions) -> indexed_storage_order
      {
        validate(dimensions);

        // _detail.resize(_logical_order.size());
        auto storage_order = ArrayTraits::template create_array<indexed_storage_type>(dimensions.size());

        for (uint32_t i = 0; i < dimensions.size(); ++i)
          {
            // Set default storage order (matches logical order).
            /// @todo Handle vector vs array
            storage_order[i] = indexed_storage_type{i, direction::ASCENDING};
          }

        return storage_order;
      }

      /**
       * Space storage order (by index).
       *
       * The dimension list must not contain duplicate dimension indices.
       *
       * @param order the storage order.
       */
      static auto
      storage_order(const logical_order& dimensions,
                    const indexed_storage_order& order) -> const indexed_storage_order&
      {
        validate(dimensions);
        validate(dimensions, order);

        return order;
      }

      /**
       * Compute storage strides for each dimension.
       *
       * This is computed for each dimension in progessing storage
       * order.
       *
       * @note The strides are not recomputed after any subsetting.
       */
      void
      compute_storage_strides()
      {
        difference_type stride = 1;

        // For each dimension in storage order, assign stride from
        // previous dimension and multiply stride by dimension size.
        // Note this is the extent size, not the subrange size.  If the
        // direction of the dimension is descending, then the stride is
        // negative to cause backward traversal.
        for (const auto& dim: _storage_order)
          {
            difference_type sign = +1;
            if (dim._direction == direction::DESCENDING)
              sign = -1;

            _detail.at(dim.index).stride = stride * sign;
            stride *= getdimension(dim.index).extent;
          }
      }

      /**
       * Compute storage origin offset.
       *
       * This is computed as the sum of the calculated offsets for
       * each descending dimension.
       *
       * @note The origin offset is not recomputed after any
       * subsetting.
       */
      void
      compute_storage_offset()
      {
        // Descending dimension offset.
        difference_type descending_offset = 0U;

        // If all dimensions are ascending, then the offset is zero.
        bool all_ascending = true;
        for (const auto& dim :_storage_order)
          {
            if (dim._direction == direction::DESCENDING)
              all_ascending = false;
          }
        if (!all_ascending)
        {
          // For each dimension in storage order where the dimension
          // is descending, compute the offset as the (extent size -1)
          // multiplied by the stride for this dimension.  The base
          // offset is the sum of the offsets for all dimensions.
          for (const auto& dim: _storage_order)
            {
              if (dim._direction == direction::DESCENDING)
                {
                  auto& detail = _detail.at(dim.index);
                  detail.descending_offset = (getdimension(dim.index).extent - 1) *
                    detail.stride;
                  // Subtract because the offset is negative due to the negative stride.
                  descending_offset -= detail.descending_offset;
                }
            }
        }

        _base = descending_offset;
      }

    public:
      /**
       * The number of dimensions in the space.
       *
       * @returns the number of dimensions.
       */
      size_type
      size() const
      {
        return _logical_order.size();
      }

      /**
       * The number of elements (product of all dimension sizes).
       *
       * @note This is the product of the subrange size of each
       * dimension, not the full range.  That is to say, it is the
       * total number of accessible elements, which might be less than
       * the total number of elements.
       *
       * @returns the number of elements.
       */
      size_type
      num_elements() const
      {
        size_type n = 1;

        for (const auto& dim : _logical_order)
          n *= dim.size();

        return n;
      }

      /**
       * Get the logical order of dimensions.
       *
       * @returns the logical order of dimensions.
       */
      const logical_order&
      get_logical_order() const
      {
        return _logical_order;
      }

      /**
       * Get the storage order of dimensions.
       *
       * @note the list is in storage order; to get the dimension
       * details, look up the dimension in its logical order by the
       * dimension index.
       *
       * @returns the storage order of dimensions.
       */
      const indexed_storage_order&
      storage_order() const
      {
        return _storage_order;
      }

      /**
       * Get a dimension by its index.
       *
       * @param dimension_index the logical index.
       * @returns a reference to the dimension.
       * @throws std::logic_error if the index is invalid.
       */
      const dimension&
      getdimension(index_type dimension_index) const
      {
        if (dimension_index >= size())
          {
            boost::format fmt("Dimension index %1% out of valid range %2%–%3%");
            fmt % dimension_index % 0 % (size() - 1);
            throw std::logic_error(fmt.str());
          }

        return _logical_order[dimension_index];
      }

      /**
       * Compute the storage index from logical coordinate.
       *
       * The logical coordinate values must be within the permitted
       * subrange defined for each dimension (indexed relative from
       * zero within the subrange).  If the indices fall outside the
       * permitted subranges, the result is undefined.
       *
       * @param coord the logical coordinate.
       * @returns the storage index corresponding to the specified
       * coordinate.
       */
      index_type
      index(const coord_type& coord) const
      {
        if (_logical_order.size() != coord.size())
          {
            boost::format fmt("Coordinate dimension count %1% does not match space dimension count %2%");
            fmt % coord.size() % size();
            throw std::logic_error(fmt.str());
          }

        difference_type pos = _base;

        // For each dimension in logical order, multiply the
        // coordinate value by the stride for the dimension and return
        // the sum for all dimensions.  When using subranges, the
        // subrange start is added to the coordinate to ensure that
        // the calculation is using coordinates in the full range,
        // rather than within the subrange window.
        for (typename list_type<dimension_type>::size_type d = 0; d < size(); ++d)
          {
            const dimension_type& dim(getdimension(d));
            const detail_type& detail = _detail.at(d);
            pos += detail.stride * (coord.at(d) + dim.begin);
          }

        return pos;
      }

      /**
       * Compute the logical coordinate from storage index.
       *
       * The storage index must correspond to a valid coordinate
       * within the permitted subrange defined for each dimension,
       * otherwise the result is undefined.
       *
       * @param index the storage index.
       * @param coord the logical coordinate corresponding to the
       * specified index.
       */
      void
      coord(index_type  index,
            coord_type& coord) const
      {
        // Note a coord reference is used rather than returning it for
        // efficiency--it avoids allocating storage for a vector which
        // is not ideal when called repeatedly.
        coord.resize(size(), 0U);

        // For each dimension in reverse logical order, compute the
        // coordinate value by dividing by the stride for the
        // dimension, then repeat using the remainder.  When using
        // subranges, the subrange start is subtracted from the
        // coordinate to ensure that the calculation is using
        // coordinates in the full range, but returning the coordinate
        // within the subrange window.
        difference_type remainder = index;
        for (typename list_type<dimension_type>::size_type d = 0; d < size(); ++d)
          {
            index_type id = size() - d - 1;
            const auto& sdim(_storage_order.at(id));
            const auto& dim(getdimension(sdim.index));
            const auto& dimdetail(_detail.at(sdim.index));

            if (dimdetail.stride < 0)
              {
                difference_type v = dim.extent * abs(dimdetail.stride);
                v -= remainder;
                v -= 1;
                coord.at(sdim.index) = v / abs(dimdetail.stride);
              }
            else
              {
                coord.at(sdim.index) = remainder / dimdetail.stride;
              }
            coord.at(sdim.index) -= dim.begin;
            remainder %= abs(dimdetail.stride);
          }
      }

      /**
       * Create a subrange from this space (by index).
       *
       * The subrange must be equal to or less than any subrange
       * already set; it is not possible to expand the range.
       *
       * @param subrange the dimensions to reduce in permitted range.
       * @returns a new space using the specified subrange.
       */
      basic_space
      subrange(const indexed_subrange_list& subrange) const
      {
        // Check for duplicates using set of indices.
        std::set<index_type> used;

        basic_space ret(*this);

        for(const auto& sub : subrange)
          {
            dimension_type& dim(ret._logical_order.at(sub.index));
            // Range is relative to existing subrange; add to existing
            // range start to make absolute.
            dim = {dim, dim.begin + sub.begin, dim.begin + sub.end};

            // Check for duplicate dimensions.
            if (used.find(sub.index) != used.end())
              {
                boost::format fmt("Dimension %1% used multiple times in subrange");
                fmt % sub.index;
                throw std::logic_error(fmt.str());
              }
            used.insert(sub.index);

            if(sub.end < sub.begin || sub.end == sub.begin ||
               sub.end > dim.extent)
              {
                boost::format fmt("Dimension %1% subrange %2%–%3% invalid");
                fmt % sub.index % sub.begin % sub.end;
                throw std::logic_error(fmt.str());
              }
          }

        return ret;
      }

      template<class charT, class StreamTraits, class SpaceContainer>
      friend std::basic_ostream<charT,StreamTraits>&
      operator<< (std::basic_ostream<charT,StreamTraits>& os,
                  const basic_space<SpaceContainer>&      space);
    };

    /**
     * Output space to output stream.
     *
     * @param os the output stream.
     * @param space the space to output.
     * @returns the output stream.
     */
    template<class charT, class StreamTraits, class SpaceContainer>
    inline std::basic_ostream<charT,StreamTraits>&
    operator<< (std::basic_ostream<charT,StreamTraits>& os,
                const basic_space<SpaceContainer>&      space)
    {
      os << ")\nLogical extents:         (";
      for (auto dim = space._logical_order.cbegin();
           dim != space._logical_order.cend();
           ++dim)
        {
          os << dim->extent;
          if (dim + 1 < space._logical_order.cend())
            os << ',';
        }
      os << ")\nLogical ranges:          (";
      for (auto dim = space._logical_order.cbegin();
           dim != space._logical_order.cend();
           ++dim)
        {
          os << '[' << dim->begin << ',' << dim->end << ')';
          if (dim + 1 < space._logical_order.cend())
            os << ',';
        }
      os << ")\nLogical sizes:           (";
      for (auto dim = space._logical_order.cbegin();
           dim != space._logical_order.cend();
           ++dim)
        {
          os << dim->size();
          if (dim + 1 < space._logical_order.cend())
            os << ',';
        }
      os << ")\nSize:                    " << space.size()
         << "\nElements:                " << space.num_elements();
      os << "\nStorage extents:         (";
      for (auto dim = space._storage_order.cbegin();
           dim != space._storage_order.cend();
           ++dim)
        {
          const auto& ldim(space.getdimension(dim->index));
          os << ldim.extent << (dim->_direction == direction::ASCENDING ? "▲" : "▼");
          if (dim + 1 < space._storage_order.cend())
            os << ',';
        }
      os << ")\nBase:                    " << space._base
         << "\nStrides:                 (";
      for (auto detail = space._detail.cbegin();
           detail != space._detail.cend();
           ++detail)
        {
          os << detail->stride;
          if (detail + 1 < space._detail.cend())
            os << ',';
        }
      os << ")\nDescending offsets:      (";
      for (auto detail = space._detail.cbegin();
           detail != space._detail.cend();
           ++detail)
        {
          os << detail->descending_offset;
          if (detail + 1 < space._detail.cend())
            os << ',';
        }
      os << ")\n";
      return os;
    }

    template<int Size>
    using array_space = basic_space<array_traits<Size>>;

    using vector_space = basic_space<vector_traits>;

  }
}

/*
 * Local Variables:
 * mode:C++
 * End:
 */
