/*
 * Copyright © 2015-2017 Roger Leigh <rleigh@codelibre.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 */

#pragma once

#include <codelibre/multiarray/dimension.h>

namespace codelibre
{
  namespace multiarray
  {

    /// Direction of dimension progression.
    enum class direction
    {
      ASCENDING, ///< Ascending progression.
      DESCENDING ///< Descending progression.
    };

    /**
     * Dimension storage specification (by index).
     *
     * Specify the storage order of a single dimension in terms of the
     * logical dimension index.
     *
     * @note Intended for use in an array where the array order
     * denotes the storage order.
     */
    template<class DimensionTraits = dimension_traits>
    struct indexed_storage
    {
      /// Logical dimension index.
      typename DimensionTraits::index_type index;
      /// Direction of dimension progression (forward or backward).
      direction _direction;
    };

    /**
     * Dimension subrange (by index).
     *
     * Restrict the range of a dimension to a subrange of its full
     * extent, as a half-open range of logical indices.
     */
    template<class DimensionTraits = dimension_traits>
    struct indexed_subrange
    {
      /// Logical dimension index.
      typename DimensionTraits::index_type index;
      /// Starting index in this dimension.
      typename DimensionTraits::index_type begin;
      /// End index in this dimension (half-open range).
      typename DimensionTraits::index_type end;
    };

    /**
     * Dimension storage details (in logical order).
     */
    template<class DimensionTraits = dimension_traits>
    struct storage_detail
    {
      /// Element stride to increment index forward by one.
      typename DimensionTraits::difference_type stride = 1;
      /// Descending offset (nonzero for descending dimensions).
      typename DimensionTraits::difference_type descending_offset = 0;
    };

  }
}

/*
 * Local Variables:
 * mode:C++
 * End:
 */
