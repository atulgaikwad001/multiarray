/*
 * Copyright © 2015-2017 Roger Leigh <rleigh@codelibre.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 */

#pragma once

#include <cstdint>
#include <string>
#include <stdexcept>
#include <iostream>

#include <boost/format.hpp>

namespace codelibre
{
  namespace multiarray
  {
    struct dimension_traits
    {
      /// Size type for dimension extents.
      using  size_type = uint32_t;
      /// Index type for dimension indices.
      using index_type = uint32_t;
      /// Signed difference type for difference between dimension indices.
      using difference_type = int32_t;
    };

    /**
     * Dimension specification.
     *
     * A description of a single dimension, at a minimum its extent (size).  It may also include a
     * subrange within the extent, used as a window to address a
     * selected subrange of the full range.
     *
     * Type definitions are for use in related classes and functions.
     */
    template<class DimensionTraits = dimension_traits>
    struct basic_dimension
    {
      /// Traits.
      using traits_type = DimensionTraits;
      /// Size type for dimension extents.
      using size_type = typename traits_type::size_type;
      /// Index type for dimension indices.
      using index_type = typename traits_type::index_type;
      /// Signed difference type for difference between dimension indices.
      using difference_type = typename traits_type::difference_type;

      /// Dimension size.
      size_type extent;
      /// Start index.
      size_type begin;
      /// End index.
      size_type end;

      basic_dimension() = delete;

      /**
       * Constructor (range).
       *
       * If the begin and end indices are both the same value, this
       * dimension exists logically fixed at the specified index, but
       * has no storage.
       *
       * @param extent the dimension size (extent).
       * @param begin the starting index.
       * @param end the ending index + 1.
       */
      basic_dimension(size_type        extent,
                      size_type        begin,
                      size_type        end):
        extent(extent),
        begin(begin),
        end(end)
      {
        if (extent == 0)
          {
            throw std::logic_error("Dimension extent is of zero length");
          }

        if (begin >= extent ||
            end > extent ||
            begin > end)
          {
            boost::format fmt("Dimension subrange %1%–%2% outside valid range %3%–%4%");
            fmt % begin % end % 0U % extent;
            throw std::logic_error(fmt.str());
          }
      }

      /**
       * Constructor (extent).
       *
       * @param extent the dimension size (extent).
       */
      basic_dimension(size_type        extent):
        basic_dimension(extent, 0U, extent)
      {
      }

      /**
       * Copy constructor.
       *
       * @param dim the dimension to copy.
       */
      basic_dimension(const basic_dimension& dim) = default;

      /**
       * Copy constructor (range).
       *
       * If the begin and end indices are both the same value, this
       * dimension exists logically fixed at the specified index, but
       * has no storage.
       *
       * @param dim the dimension to copy.
       * @param begin the starting index.
       * @param end the ending index + 1.
       */
      basic_dimension(const basic_dimension& dim,
                      const size_type  begin,
                      const size_type  end):
        basic_dimension(dim.extent, begin, end)
      {
      if (begin < dim.begin ||
          end > dim.end)
        {
          boost::format fmt("Dimension subrange %1%–%2% outside valid subrange %3%–%4%");
          fmt % begin % end % dim.begin % dim.end;
          throw std::logic_error(fmt.str());
        }
      }

      /// Destructor.
      ~basic_dimension() = default;

      /**
       * Get usable size.
       *
       * This is determined by the begin and end values defining the
       * usable range.
       *
       * @note Use extent for the full range.
       *
       * @returns the usable size.
       */
      size_type
      size() const
      {
        return end - begin;
      }

    };

    using dimension = basic_dimension<>;

  }
}

/*
 * Local Variables:
 * mode:C++
 * End:
 */
