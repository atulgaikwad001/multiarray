/*
 * Copyright © 2015-2017 Roger Leigh <rleigh@codelibre.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 */

#include <set>

#include <codelibre/multiarray/dimension.h>

namespace codelibre
{
  namespace multiarray
  {

    constexpr void
    Dimension::check(size_type previous_begin,
                     size_type previous_end)
    {
      if (extent == 0)
        {
          boost::format fmt("Dimension %1% extent is of zero length");
          fmt % name;
          throw std::logic_error(fmt.str());
        }

      if (begin >= extent ||
          end > extent ||
          begin > end)
        {
          boost::format fmt("Dimension %1% subrange %2%–%3% outside valid range %4%–%5%");
          fmt % name % begin % end % 0U % extent;
          throw std::logic_error(fmt.str());
        }

      if (begin < previous_begin ||
          end > previous_end)
        {
          boost::format fmt("Dimension %1% subrange %2%–%3% outside valid subrange %4%–%5%");
          fmt % name % begin % end % previous_begin % previous_end;
          throw std::logic_error(fmt.str());
        }
    }

  }
}
