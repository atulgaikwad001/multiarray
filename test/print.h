/*
 * Copyright © 2015-2018 Roger Leigh <rleigh@codelibre.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 */

#pragma once

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>

#include <boost/range/size.hpp>

#include <codelibre/multiarray/storage.h>
#include <codelibre/multiarray/util.h>

using codelibre::multiarray::dimension;
using codelibre::multiarray::direction;
using codelibre::multiarray::array_space;
using codelibre::multiarray::vector_space;
using codelibre::multiarray::indexed_storage;
using codelibre::multiarray::indexed_subrange;
using codelibre::multiarray::make_logical_space;

namespace test
{

    // Print a 2D array.
    // planecoord is the index into the higher dimension (>=2) indices;
    // the lowest two dimensions will be rendered in a 2D grid.
    inline void dump_array(const vector_space& space,
                    const std::vector<std::string>& labels,
                    vector_space::coord_type planecoord,
                    uint32_t index_size)
    {
        if (space.size() == 0)
            return;

        uint64_t ylabsize = 1;
        uint64_t yvalsize = 1;

        const auto& logical = space.get_logical_order();

        uint32_t nx = logical.at(0).size();;
        uint32_t ny = 1;

        if (space.size() > 1)
        {
            ylabsize = labels.at(1).size();
            ny = logical.at(1).size();

            yvalsize = static_cast<uint32_t>(std::floor(std::log10(static_cast<float>(logical.at(1).size()-1)))) + 1;
        }

        // Header
        std::string sepy;
        std::string sepy2;
        if (space.size() > 1)
        {
            sepy = std::string(ylabsize + yvalsize + 2, ' ');
            sepy2 = std::string(ylabsize + 1, ' ');
        }
        std::string xthickline;
        std::string xthinline;
        std::string ythickline;
        std::string ythinline;
        for (uint32_t i = 0; i < index_size; ++i)
        {
            xthickline += "━";
            xthinline += "─";
        }
        for (uint32_t i = 0; i < yvalsize; ++i)
        {
            ythickline += "━";
            ythinline += "─";
        }

        // Column label
        std::cout << sepy << labels.at(0) << '\n';

        // Top line
        std::cout << sepy << "┏";
        for (uint32_t i = 0; i < nx; ++i)
        {
            std::cout << xthickline;
            if (i + 1 < nx)
                std::cout << "┯";
        }
        std::cout << "┓\n";

        // Column values
        std::cout << sepy << "┃";
        for (uint32_t i = 0; i < nx; ++i)
        {
            std::cout << std::setw(index_size) << std::right << i;
            if (i + 1 < nx)
                std::cout << "│";
        }
        std::cout << "┃\n";

        // Mid line
        if (space.size() > 1)
        {
            std::cout << labels.at(1) << ' ' << "┏";
            for (uint32_t i = 0; i < 1; ++i)
            {
                std::cout << ythickline;
                if (i + 1 < 1)
                    std::cout << "┯";
            }
            std::cout << "╋";
            for (uint32_t i = 0; i < nx; ++i)
            {
                std::cout << xthickline;
                if (i + 1 < nx)
                    std::cout << "┿";
            }
            std::cout << "┫\n";
        }
        else
        {
            std::cout << sepy << "┣";
            for (uint32_t i = 0; i < nx; ++i)
            {
                std::cout << xthickline;
                if (i + 1 < nx)
                    std::cout << "┿";
            }
            std::cout << "┫\n";
        }

        vector_space::coord_type index;
        index.push_back(0);
        if (space.size() > 1)
            index.push_back(0);
        std::copy(planecoord.begin(), planecoord.end(),
                  std::back_inserter(index));

        // Loop over rows.
        if (space.size() > 1)
        {
            for (uint32_t y = 0; y < ny; ++y)
            {
                if (space.size() > 1)
                    index.at(1) = y;

                // Row values
                if (space.size() > 1)
                {
                    std::cout << sepy2 << "┃";
                    for (uint32_t i = 0; i < 1; ++i)
                    {
                        std::cout << std::setw(yvalsize) << std::right << y;
                        if (i + 1 < 1)
                            std::cout << "│";
                    }
                    std::cout << "┃";
                    for (uint32_t x = 0; x < nx; ++x)
                    {
                        index.at(0) = x;
                        std::cout << std::setw(index_size) << space.index(index);
                        if (x + 1 < nx)
                            std::cout << "│";
                    }
                    std::cout << "┃\n";
                }

                // Row line
                if (y + 1 < ny && space.size() > 1)
                {
                    std::cout << sepy2 << "┠";
                    for (uint32_t i = 0; i < 1; ++i)
                    {
                        std::cout << ythinline;
                        if (i + 1 < 1)
                            std::cout << "┼";
                    }
                    std::cout << "╂";
                    for (uint32_t i = 0; i < nx; ++i)
                    {
                        std::cout << xthinline;
                        if (i + 1 < nx)
                            std::cout << "┼";
                    }
                    std::cout << "┨\n";
                }
            }
        }
        else
        {
            std::cout << sepy << "┃";
            for (uint32_t x = 0; x < nx; ++x)
            {
                index.at(0) = x;
                std::cout << std::setw(index_size) << space.index(index);
                if (x + 1 < nx)
                    std::cout << "│";
            }
            std::cout << "┃\n";
        }

        // Bottom line
        if (space.size() > 1)
        {
            std::cout << sepy2 << "┗";
            for (uint32_t i = 0; i < 1; ++i)
            {
                std::cout << ythickline;
                if (i + 1 < 1)
                    std::cout << "┷";
            }
            std::cout << "┻";
            for (uint32_t i = 0; i < nx; ++i)
            {
                std::cout << xthickline;
                if (i + 1 < nx)
                    std::cout << "┷";
            }
            std::cout << "┛\n";
        }
        else
        {
            std::cout << sepy << "┗";
            for (uint32_t i = 0; i < nx; ++i)
            {
                std::cout << xthickline;
                if (i + 1 < nx)
                    std::cout << "┷";
            }
            std::cout << "┛\n";
        }
    }

    // Print an nD array.
    inline void
    dump_array(const vector_space& space)
    {
        const vector_space logical_space(make_logical_space(space, true, true));

      auto labels = std::vector<std::string>();
      for (decltype(space.size()) i = 0;
           i < space.size();
           ++i) {
        std::ostringstream l;
        l << 'D' << i;
        labels.push_back(l.str());
      }

        // Maximum length of index for display.
        uint32_t index_size = 1;
        {
            vector_space::coord_type idx;
            for(dimension::index_type i = 0; i < space.num_elements(); ++i)
            {
                logical_space.coord(i, idx);
                index_size = std::max(index_size,
                                      static_cast<uint32_t>(std::floor(std::log10(static_cast<float>(space.index(idx))))) + 1);
            }
        }

        if (space.size() < 3) // Render as 1D or 2D plane
        {
            vector_space::coord_type planecoord; // Intentionally empty.
            dump_array(space, labels, planecoord, index_size);
        }
        else // Render as multiple 2D planes
        {
            const auto& logical = logical_space.get_logical_order();

            std::vector<dimension> dims;
            for (uint32_t i = 2; i < logical.size(); ++i)
                dims.push_back(logical.at(i));
            vector_space space2(dims);

            vector_space::coord_type coord;
            for(dimension::index_type i = 0; i < space2.num_elements(); ++i)
            {
                space2.coord(i, coord);
                for (uint32_t j = 0; j < coord.size(); ++j)
                {
                    const dimension& dim(logical.at(j + 2));
                    std::cout << labels.at(j+2) << '=' << coord.at(j);
                    if (j + 1 < coord.size())
                        std::cout << ", ";
                }
                std::cout << '\n';
                dump_array(space, labels, coord, index_size);
                std::cout << '\n';
            }
        }
    }

    // Print a vector-type object with comma separators.
    template<typename T>
    struct dump_vector
    {
        const T& v;

        explicit dump_vector(const T& v):
                v(v)
        {}
    };

    template<typename T>
    std::ostream&
    operator << (std::ostream& os, const dump_vector<T>& vec)
    {
        for (auto i = vec.v.begin();
             i != vec.v.end();
             ++i)
        {
            os << *i;
            if (i + 1 != vec.v.end())
                os << ',';
        }
        return os;
    }


}

/*
 * Local Variables:
 * mode:C++
 * End:
 */
